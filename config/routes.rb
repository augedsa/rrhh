Rrhh::Application.routes.draw do
  get "estadisticas/index"
  get "estadisticas/indiv"
  scope '(:locale)' do
    resources :ciudades
    resources :permisologia_configuraciones
    resources :permisologias_menu
    resources :configuraciones
    resources :menus
    resources :objetivos
    resources :estados
    resources :proyectos do
      get :autocomplete_prospecto_apellido, :on => :collection
      get :autocomplete_usuario, :on => :collection
    end
    get "calendario/index"
    resources :cartas do
      get :autocomplete_prospecto_apellido, :on => :collection
    end
    get 'upload' => 'cartas'
    get 'obj_proyecto' => 'objetivos'
    post 'upload_file' => 'cartas'
    get 'config_func' => 'admin'
    get 'permisologia' => 'roles'
    post 'updatePermisologia' => 'roles'
    resources :notas_telefonicas do
      get :autocomplete_prospecto_apellido, :on => :collection
    end
    resources :usuarios
    resources :roles
    resources :cargos
    resources :prospectos do
      get :autocomplete_prospecto_apellido, :on => :collection
    end
    resources :edociviles
    resources :tipos
    resources :tipologias
    resources :entrenamientos
    resources :habilidades
    resources :areas
    resources :organizaciones
    get 'admin' => 'admin#index'
    controller :sesiones do
      get 'login' => :new
      post 'login' => :create
      delete 'logout' => :destroy
    end
    resources :usuarios
    get 'prospectos/filtrar_prospectos'
    get 'inicio' => 'inicio#olvido_clave'
    post 'inicio' => 'inicio#olvido_clave'
    root 'sesiones#new'
    # The priority is based upon order of creation: first created -> highest priority.
    # See how all your routes lay out with "rake routes".

    # You can have the root of your site routed with "root"


    # Example of regular route:
    #   get 'products/:id' => 'catalog#view'

    # Example of named route that can be invoked with purchase_url(id: product.id)
    #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

    # Example resource route (maps HTTP verbs to controller actions automatically):
    #   resources :products

    # Example resource route with options:
    #   resources :products do
    #     member do
    #       get 'short'
    #       post 'toggle'
    #     end
    #
    #     collection do
    #       get 'sold'
    #     end
    #   end

    # Example resource route with sub-resources:
    #   resources :products do
    #     resources :comments, :sales
    #     resource :seller
    #   end

    # Example resource route with more complex sub-resources:
    #   resources :products do
    #     resources :comments
    #     resources :sales do
    #       get 'recent', on: :collection
    #     end
    #   end

    # Example resource route with concerns:
    #   concern :toggleable do
    #     post 'toggle'
    #   end
    #   resources :posts, concerns: :toggleable
    #   resources :photos, concerns: :toggleable

    # Example resource route within a namespace:
    #   namespace :admin do
    #     # Directs /admin/products/* to Admin::ProductsController
    #     # (app/controllers/admin/products_controller.rb)
    #     resources :products
    #   end
  end
end
