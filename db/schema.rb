# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140421014412) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "areas", force: true do |t|
    t.string   "nombre"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "cartas", force: true do |t|
    t.integer  "usuario_id"
    t.integer  "prospecto_id"
    t.datetime "fecha"
    t.text     "cuerpo"
    t.boolean  "respuesta"
    t.text     "url_adjunto"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "cartas", ["prospecto_id"], name: "index_cartas_on_prospecto_id", using: :btree
  add_index "cartas", ["usuario_id"], name: "index_cartas_on_usuario_id", using: :btree

  create_table "ciudades", force: true do |t|
    t.string   "nombre"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "configuraciones", force: true do |t|
    t.string   "nombre_configuracion"
    t.string   "path_configuracion"
    t.string   "controlador_configuracion"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "edociviles", force: true do |t|
    t.string   "nombre"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "entrenamientos", force: true do |t|
    t.string   "nombre"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "estados", force: true do |t|
    t.string   "nombre"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "habilidades", force: true do |t|
    t.string   "nombre"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "menus", force: true do |t|
    t.string   "span_menu"
    t.string   "class_menu"
    t.string   "path_menu"
    t.string   "id_li_menu"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "controlador_menu"
  end

  create_table "notas_telefonicas", force: true do |t|
    t.integer  "usuario_id"
    t.integer  "prospecto_id"
    t.datetime "fecha"
    t.text     "nota_descripcion"
    t.datetime "fecha_recordatorio"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "recordatorio"
    t.boolean  "visitado"
  end

  add_index "notas_telefonicas", ["prospecto_id"], name: "index_notas_telefonicas_on_prospecto_id", using: :btree
  add_index "notas_telefonicas", ["usuario_id"], name: "index_notas_telefonicas_on_usuario_id", using: :btree

  create_table "objetivos", force: true do |t|
    t.string   "titulo"
    t.text     "cuerpo"
    t.datetime "fecha_inicio"
    t.datetime "fecha_fin"
    t.integer  "estado_id"
    t.integer  "proyecto_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "objetivos", ["estado_id"], name: "index_objetivos_on_estado_id", using: :btree
  add_index "objetivos", ["proyecto_id"], name: "index_objetivos_on_proyecto_id", using: :btree

  create_table "organizaciones", force: true do |t|
    t.string   "nombre"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "permisologia_configuraciones", force: true do |t|
    t.integer  "rol_id"
    t.integer  "configuracion_id"
    t.boolean  "permiso"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "permiso_new"
    t.boolean  "permiso_edit"
    t.boolean  "permiso_delete"
  end

  add_index "permisologia_configuraciones", ["configuracion_id"], name: "index_permisologia_configuraciones_on_configuracion_id", using: :btree
  add_index "permisologia_configuraciones", ["rol_id"], name: "index_permisologia_configuraciones_on_rol_id", using: :btree

  create_table "permisologias_menu", force: true do |t|
    t.integer  "rol_id"
    t.integer  "menu_id"
    t.boolean  "permiso"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "permiso_new"
    t.boolean  "permiso_edit"
    t.boolean  "permiso_delete"
  end

  add_index "permisologias_menu", ["menu_id"], name: "index_permisologias_menu_on_menu_id", using: :btree
  add_index "permisologias_menu", ["rol_id"], name: "index_permisologias_menu_on_rol_id", using: :btree

  create_table "prospectos", force: true do |t|
    t.string   "nombre"
    t.string   "direccion"
    t.string   "telefono"
    t.string   "correo"
    t.integer  "habilidad_id"
    t.integer  "entrenamiento_id"
    t.integer  "organizacion_id"
    t.integer  "tipo_id"
    t.integer  "edad"
    t.integer  "edocivil_id"
    t.integer  "hijos"
    t.integer  "deudas"
    t.integer  "area_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "apellido"
    t.integer  "ciudad_id"
    t.integer  "zona_postal"
    t.integer  "tipologia_id"
    t.boolean  "public"
    t.integer  "organizacionpublic_id"
    t.integer  "usuario_id"
  end

  add_index "prospectos", ["area_id"], name: "index_prospectos_on_area_id", using: :btree
  add_index "prospectos", ["ciudad_id"], name: "index_prospectos_on_ciudad_id", using: :btree
  add_index "prospectos", ["edocivil_id"], name: "index_prospectos_on_edocivil_id", using: :btree
  add_index "prospectos", ["entrenamiento_id"], name: "index_prospectos_on_entrenamiento_id", using: :btree
  add_index "prospectos", ["habilidad_id"], name: "index_prospectos_on_habilidad_id", using: :btree
  add_index "prospectos", ["organizacion_id"], name: "index_prospectos_on_organizacion_id", using: :btree
  add_index "prospectos", ["organizacionpublic_id"], name: "index_prospectos_on_organizacionpublic_id", using: :btree
  add_index "prospectos", ["tipo_id"], name: "index_prospectos_on_tipo_id", using: :btree
  add_index "prospectos", ["tipologia_id"], name: "index_prospectos_on_tipologia_id", using: :btree
  add_index "prospectos", ["usuario_id"], name: "index_prospectos_on_usuario_id", using: :btree

  create_table "proyectos", force: true do |t|
    t.string   "nombre"
    t.integer  "prospecto_id"
    t.integer  "usuario_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "proyectos", ["prospecto_id"], name: "index_proyectos_on_prospecto_id", using: :btree
  add_index "proyectos", ["usuario_id"], name: "index_proyectos_on_usuario_id", using: :btree

  create_table "roles", force: true do |t|
    t.string   "nombre"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "tipologias", force: true do |t|
    t.string   "nombre"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "tipos", force: true do |t|
    t.string   "nombre"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "usuarios", force: true do |t|
    t.string   "nombre_usuario"
    t.string   "nombre_completo"
    t.string   "password_digest"
    t.string   "correo"
    t.integer  "rol_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "usuarios", ["rol_id"], name: "index_usuarios_on_rol_id", using: :btree

end
