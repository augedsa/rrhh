class CreateProyectos < ActiveRecord::Migration
  def change
    create_table :proyectos do |t|
      t.string :nombre
      t.references :prospecto, index: true
      t.references :usuario, index: true

      t.timestamps
    end
  end
end
