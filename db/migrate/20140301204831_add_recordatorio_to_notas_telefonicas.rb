class AddRecordatorioToNotasTelefonicas < ActiveRecord::Migration
  def change
    add_column :notas_telefonicas, :recordatorio, :boolean
  end
end
