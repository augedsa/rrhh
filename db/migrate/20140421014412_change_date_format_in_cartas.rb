class ChangeDateFormatInCartas < ActiveRecord::Migration
  def change
	change_column :cartas, :fecha,  :timestamp
	change_column :objetivos, :fecha_inicio,  :timestamp
	change_column :objetivos, :fecha_fin,  :timestamp
  end
end
