class AddOrganizacionPublicToProspectos < ActiveRecord::Migration
  def change
    add_column :prospectos, :public, :boolean
    add_reference :prospectos, :organizacionpublic, index: true
  end
end
