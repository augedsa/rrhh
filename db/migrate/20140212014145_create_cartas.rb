class CreateCartas < ActiveRecord::Migration
  def change
    create_table :cartas do |t|
      t.references :usuario, index: true
      t.references :prospecto, index: true
      t.date :fecha
      t.text :cuerpo
      t.boolean :respuesta
      t.text :url_adjunto

      t.timestamps
    end
  end
end
