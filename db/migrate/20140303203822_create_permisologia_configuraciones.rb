class CreatePermisologiaConfiguraciones < ActiveRecord::Migration
  def change
    create_table :permisologia_configuraciones do |t|
      t.references :rol, index: true
      t.references :configuracion, index: true
      t.boolean :permiso

      t.timestamps
    end
  end
end
