class AddIdUsuarioToProspectos < ActiveRecord::Migration
  def change
    add_reference :prospectos, :usuario, index: true
  end
end
