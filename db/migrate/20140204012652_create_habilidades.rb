class CreateHabilidades < ActiveRecord::Migration
  def change
    create_table :habilidades do |t|
      t.string :nombre

      t.timestamps
    end
  end
end
