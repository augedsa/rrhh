class CreateTipologias < ActiveRecord::Migration
  def change
    create_table :tipologias do |t|
      t.string :nombre

      t.timestamps
    end
  end
end
