class CreateNotasTelefonicas < ActiveRecord::Migration
  def change
    create_table :notas_telefonicas do |t|
      t.references :usuario, index: true
      t.references :prospecto, index: true
      t.timestamp :fecha
      t.text :nota_descripcion
      t.timestamp :fecha_recordatorio

      t.timestamps
    end
  end
end
