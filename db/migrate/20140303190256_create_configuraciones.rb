class CreateConfiguraciones < ActiveRecord::Migration
  def change
    create_table :configuraciones do |t|
      t.string :nombre_configuracion
      t.string :path_configuracion
      t.string :controlador_configuracion

      t.timestamps
    end
  end
end
