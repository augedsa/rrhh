class CreatePermisologiasMenu < ActiveRecord::Migration
  def change
    create_table :permisologias_menu do |t|
      t.references :rol, index: true
      t.references :menu, index: true
      t.boolean :permiso

      t.timestamps
    end
  end
end
