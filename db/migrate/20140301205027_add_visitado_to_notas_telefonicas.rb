class AddVisitadoToNotasTelefonicas < ActiveRecord::Migration
  def change
    add_column :notas_telefonicas, :visitado, :boolean
  end
end
