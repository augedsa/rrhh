class CreateProspectos < ActiveRecord::Migration
  def change
    create_table :prospectos do |t|
      t.string :nombre
      t.string :direccion
      t.string :telefono
      t.string :correo
      t.references :habilidad, index: true
      t.references :entrenamiento, index: true
      t.boolean :staff
      t.boolean :exstaff
      t.references :organizacion, index: true
      t.references :tipo, index: true
      t.integer :edad
      t.references :edocivil, index: true
      t.integer :hijos
      t.integer :deudas
      t.references :area, index: true

      t.timestamps
    end
  end
end
