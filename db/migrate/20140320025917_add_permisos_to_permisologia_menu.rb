class AddPermisosToPermisologiaMenu < ActiveRecord::Migration
  def change
    add_column :permisologias_menu, :permiso_new, :boolean
    add_column :permisologias_menu, :permiso_edit, :boolean
    add_column :permisologias_menu, :permiso_delete, :boolean
  end
end
