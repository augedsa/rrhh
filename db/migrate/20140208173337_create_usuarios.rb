class CreateUsuarios < ActiveRecord::Migration
  def change
    create_table :usuarios do |t|
      t.string :nombre_usuario
      t.string :nombre_completo
      t.string :password_digest
      t.string :correo
      t.references :rol, index: true

      t.timestamps
    end
  end
end
