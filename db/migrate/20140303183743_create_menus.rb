class CreateMenus < ActiveRecord::Migration
  def change
    create_table :menus do |t|
      t.string :span_menu
      t.string :class_menu
      t.string :path_menu
      t.string :id_li_menu

      t.timestamps
    end
  end
end
