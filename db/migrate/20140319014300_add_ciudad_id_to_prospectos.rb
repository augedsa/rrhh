class AddCiudadIdToProspectos < ActiveRecord::Migration
  def change
    add_reference :prospectos, :ciudad, index: true
  end
end
