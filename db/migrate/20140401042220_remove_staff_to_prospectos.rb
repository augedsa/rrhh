class RemoveStaffToProspectos < ActiveRecord::Migration
  def change
    remove_column :prospectos, :staff, :boolean
    remove_column :prospectos, :exstaff, :boolean
  end
end
