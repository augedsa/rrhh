class CreateEntrenamientos < ActiveRecord::Migration
  def change
    create_table :entrenamientos do |t|
      t.string :nombre

      t.timestamps
    end
  end
end
