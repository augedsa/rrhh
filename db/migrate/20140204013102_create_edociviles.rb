class CreateEdociviles < ActiveRecord::Migration
  def change
    create_table :edociviles do |t|
      t.string :nombre

      t.timestamps
    end
  end
end
