class AddPermisosToPermisologiaConfiguracion < ActiveRecord::Migration
  def change
    add_column :permisologia_configuraciones, :permiso_new, :boolean
    add_column :permisologia_configuraciones, :permiso_edit, :boolean
    add_column :permisologia_configuraciones, :permiso_delete, :boolean
  end
end
