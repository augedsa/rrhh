class CreateObjetivos < ActiveRecord::Migration
  def change
    create_table :objetivos do |t|
      t.string :titulo
      t.text :cuerpo
      t.date :fecha_inicio
      t.date :fecha_fin
      t.references :estado, index: true
      t.references :proyecto, index: true

      t.timestamps
    end
  end
end
