class AddTipologiaToProspectos < ActiveRecord::Migration
  def change
    add_reference :prospectos, :tipologia, index: true
  end
end
