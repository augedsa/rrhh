class PdfGenerator< Prawn::Document

  def initialize()
    super()
  end

  def show_prospecto(prospecto)
    text "Prospetto", :align => :center, :size => 24
    text "#{prospecto.apellido_nombre}", :align => :center, :size => 20, :style => :bold
    bounding_box([50, cursor], :width => 450) do
      # transparent(0.5) { stroke_bounds }
      cuidad = ""
      edocivil = ""
      habilidad = ""
      entrenamiento = ""
      organizacion = ""
      tipo = ""
      area = ""
      unless prospecto.ciudad.blank?
        ciudad = prospecto.ciudad.nombre
      else
        ciudad = "Nessun valore"
      end
      unless prospecto.edocivil.blank?
        edocivil = prospecto.edocivil.nombre
      else
        edocivil = "Nessun valore"
      end
      unless prospecto.habilidad.blank?
        habilidad = prospecto.habilidad.nombre
      else
        habilidad = "Nessun valore"
      end
      unless prospecto.entrenamiento.blank?
        entrenamiento = prospecto.entrenamiento.nombre
      else
        entrenamiento = "Nessun valore"
      end
      unless prospecto.organizacion.blank?
        organizacion = prospecto.organizacion.nombre
      else
        organizacion = "Nessun valore"
      end
      unless prospecto.tipo.blank?
        tipo = prospecto.tipo.nombre
      else
        tipo = "Nessun valore"
      end
      unless prospecto.area.blank?
        area = prospecto.area.nombre
      else
        area = "Nessun valore"
      end
      font_size 14
      move_down 7
      pad(5) { text "Cognome: #{prospecto.apellido}" }
      stroke_horizontal_rule
      move_down 7
      pad(5) { text "Nome: #{prospecto.nombre}" }
      stroke_horizontal_rule
      move_down 7
      pad(5) { text "Indirizzo: #{prospecto.direccion}" }
      stroke_horizontal_rule
      move_down 7
      pad(5) { text "Città: #{ciudad}" }
      stroke_horizontal_rule
      move_down 7
      pad(5) { text "Codice Postale: #{prospecto.zona_postal}" }
      stroke_horizontal_rule
      move_down 7
      pad(5) { text "Teléfono: #{prospecto.telefono}" }
      stroke_horizontal_rule
      move_down 7
      pad(5) { text "Mail: #{prospecto.correo}" }
      stroke_horizontal_rule
      move_down 7
      pad(5) { text "Livello di caso: #{habilidad}" }
      stroke_horizontal_rule
      move_down 7
      pad(5) { text "Livello di addestramento: #{entrenamiento}" }
      stroke_horizontal_rule
      move_down 7
      pad(5) { text "Organizzazione: #{organizacion}" }
      stroke_horizontal_rule
      move_down 7
      pad(5) { text "Tipo: #{tipo}" }
      stroke_horizontal_rule
      move_down 7
      pad(5) { text "Età: #{prospecto.edad}" }
      stroke_horizontal_rule
      move_down 7
      pad(5) { text "Stato Civile: #{edocivil}" }
      stroke_horizontal_rule
      move_down 7
      pad(5) { text "Figli: #{prospecto.hijos}" }
      stroke_horizontal_rule
      move_down 7
      pad(5) { text "Debiti: #{prospecto.deudas} €" }
      stroke_horizontal_rule
      move_down 7
      pad(5) { text "Professione: #{area}" }
      stroke_horizontal_rule
    end
  end

  def list_prospecto(prospectos, cantidad)
    text "Elenco Prospetti", :align => :center, :size => 24
    move_down 20

    data = [["Cognome","Nome","Età","St. Civile","Figli","Debiti","Città","Telefono","Mail"]]
    prospectos.each do |prospecto|
      font_size 9
      cuidad = ""
      edocivil = ""
      unless prospecto.ciudad.blank?
        ciudad = prospecto.ciudad.nombre
      else
        ciudad = "Nessun valore"
      end
      unless prospecto.edocivil.blank?
        edocivil = prospecto.edocivil.nombre
      else
        edocivil = "Nessun valore"
      end
      data += [[prospecto.apellido,prospecto.nombre,prospecto.edad,edocivil,prospecto.hijos,prospecto.deudas,ciudad,prospecto.telefono,prospecto.correo]]
    end
    table(data, :header => true, :row_colors => ["F0F0F0", "FDFDFD"], :position => :center, :column_widths => [70, 70, 31, 55, 30, 36, 70])

    pad(5) {text "Totale Prospetti: " + cantidad.to_s}
  end
end
