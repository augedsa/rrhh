module NotasTelefonicasHelper
  def ProspectoDetail(prospectoID)
	  prospecto = Prospecto.find_by(id: prospectoID)
	  deuda = ""
	  exstaff = ""
	  edocivil = "Nessun valore"
	  ciudad = "Nessun valore"
	  if prospecto.deudas > 0
	  	deuda = "<span class='glyphicon glyphicon-plus-sign' style='color:red;'></span>"
	  else
	  	deuda = "<span class='glyphicon glyphicon-minus-sign' style='color:green;'></span>"
	  end
	  unless prospecto.edocivil.blank?
	  	edocivil = prospecto.edocivil.nombre
	  end
  	  unless prospecto.ciudad.blank?
  	  	ciudad = prospecto.ciudad.nombre
  	  end
	  "<b>Prospetto</b><br />
		  <div class='table-responsive'>
		    <table class='table table-hover'>
		    <thead>
		      <tr>
			      <th>Cognome</th>
			      <th>Nome</th>
			      <th>Età</th>
			      <th>Stato Civile</th>
			      <th>Figli</th>
			      <th>Debiti</th>
			      <th>Città</th>
			      <th>Teléfono</th>
			      <th>Mail</th>
			      <th></th>
		      </tr>
		    </thead>
		    <tbody>
		        <tr>
		          <td>#{prospecto.apellido}</td>
		          <td>#{prospecto.nombre}</td>
		          <td>#{prospecto.edad}</td>
		          <td>#{edocivil }</td>
		          <td>#{prospecto.hijos }</td>
		          <td>#{deuda}</td>
		          <td>#{ciudad}</td>
		          <td>#{prospecto.telefono }</td>
		          <td>#{prospecto.correo }</td>
		          <td>#{link_to content_tag(:span, '', class: 'glyphicon glyphicon-folder-open'), prospecto, title: 'Mostrare', id: 'acciones' }</td>
		        </tr>
		    </tbody>
		    </table>
		  </div>".html_safe
  end
end
