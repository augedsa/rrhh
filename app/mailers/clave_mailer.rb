class ClaveMailer < ActionMailer::Base
  default from: "Antonio Cardinale <antonio.cardinale@hotmail.com>"

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.clave.recuperar.subject
  #
  def olvido_clave(usuario,clave_nueva)  
    @clave = clave_nueva
    mail(to: usuario.correo, subject: "Nueva Clave de Inicio de Sesión", from: "rrhh")  

  end 
end
