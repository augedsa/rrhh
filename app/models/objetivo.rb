class Objetivo < ActiveRecord::Base
  validates :titulo, :cuerpo, presence: true
  belongs_to :estado
  belongs_to :proyecto
end
