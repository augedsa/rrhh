class Usuario < ActiveRecord::Base
	validates :nombre_usuario, :nombre_completo, :correo, :password_digest, presence: true
	validates :nombre_usuario, uniqueness: true
	validates :correo, format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i , message: "formato non valido"}
	belongs_to :rol
	has_secure_password
	after_destroy :ensure_an_admin_remains
	has_many :nota_telefonicas
	has_many :carta
	has_many :proyecto
	has_many :prospecto

	def self.encriptar(password_digest)
		BCrypt::Password.create(password_digest)
	end

	private
	def ensure_an_admin_remains
		if Usuario.count.zero?
			raise "No se puede eliminar el último usuario"
		end
	end
end
