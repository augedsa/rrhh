
class Prospecto < ActiveRecord::Base
  validates :nombre, :direccion, :telefono, :correo, :edad, :hijos, :deudas, presence: true
  validates :correo, format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i , message: "formato non valido"}
  validates :edad, numericality: { only_integer: true, greater_than: 15 }
  validates :hijos, numericality: { only_integer: true, greater_than_or_equal_to: 0 }
  validates :deudas, numericality: { only_integer: true, greater_than_or_equal_to: 0 }
  belongs_to :habilidad
  belongs_to :entrenamiento
  belongs_to :organizacion, :class_name => "Organizacion"
  belongs_to :organizacionpublic, :class_name => "Organizacion"
  belongs_to :tipo
  belongs_to :edocivil
  belongs_to :area
  belongs_to :ciudad
  belongs_to :tipologia
  belongs_to :usuario
  validates :nombre, :direccion, :telefono, :correo, :edad, :hijos, :deudas, presence: true
  has_many :notas_telefonicas, :dependent => :delete_all
  has_many :cartas, :dependent => :delete_all
  has_many :proyectos, :dependent => :destroy

def apellido_nombre
  self.apellido+' '+self.nombre
end

end

