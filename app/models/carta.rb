class Carta < ActiveRecord::Base
  validates :cuerpo,:prospecto, presence: true, on: :create
  belongs_to :usuario
  belongs_to :prospecto
end
