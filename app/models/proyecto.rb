class Proyecto < ActiveRecord::Base
  belongs_to :prospecto
  belongs_to :usuario
  has_many :objetivos, :dependent => :delete_all
  #after_create :objDefault
  validates :nombre,:prospecto_id, presence: true

  #Objetivo por defecto
  # def objDefault
  # 	objetivo = Objetivo.new
  # 	objetivo.proyecto_id = self.id
  # 	objetivo.estado_id = 1
  # 	objetivo.save
  # end
end
