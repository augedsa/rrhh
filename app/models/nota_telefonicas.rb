class NotaTelefonicas < ActiveRecord::Base
  validates :nota_descripcion,:prospecto, presence: true
  belongs_to :usuario
  belongs_to :prospecto
end
