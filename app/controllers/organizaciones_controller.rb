class OrganizacionesController < ApplicationController
  before_action :set_organizacion, only: [:show, :edit, :update, :destroy]

  # GET /organizaciones
  # GET /organizaciones.json
  def index
    @organizaciones = Organizacion.paginate(:page => params[:page], :per_page => 10).order('nombre ASC')
    @cantidad = @organizaciones.count
  end

  # GET /organizaciones/1
  # GET /organizaciones/1.json
  def show
  end

  # GET /organizaciones/new
  def new
    @organizacion = Organizacion.new
  end

  # GET /organizaciones/1/edit
  def edit
  end

  # POST /organizaciones
  # POST /organizaciones.json
  def create
    @organizacion = Organizacion.new(organizacion_params)

    respond_to do |format|
      if @organizacion.save
        format.html { redirect_to @organizacion, notice: 'Organizzazione è stato creato con successo.' }
        format.json { render action: 'show', status: :created, location: @organizacion }
      else
        format.html { render action: 'new' }
        format.json { render json: @organizacion.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /organizaciones/1
  # PATCH/PUT /organizaciones/1.json
  def update
    respond_to do |format|
      if @organizacion.update(organizacion_params)
        format.html { redirect_to @organizacion, notice: 'Organizzazione è stato aggiornato con successo.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @organizacion.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /organizaciones/1
  # DELETE /organizaciones/1.json
  def destroy
    @organizacion.destroy
    respond_to do |format|
      format.html { redirect_to organizaciones_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_organizacion
      @organizacion = Organizacion.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def organizacion_params
      params.require(:organizacion).permit(:nombre)
    end
end
