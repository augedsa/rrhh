class CartasController < ApplicationController
  before_action :set_carta, only: [:show, :edit, :update, :destroy]

  # autocomplete :prospecto, :apellido,:extra_data => [:id]
  def autocomplete_prospecto_apellido
    term = params[:term].upcase
    prospectos = Prospecto.where("upper(apellido) LIKE ? or upper(nombre) LIKE ? or upper(concat(apellido,' ', nombre)) like ?","%#{term}%","%#{term}%","%#{term}%").order(:apellido)
    #prospectos = Prospecto.where("apellido LIKE ?","%#{term}%").order(:apellido)
    render :json => prospectos.map { |prospecto| {:id => prospecto.id, :label => prospecto.apellido_nombre, :value => prospecto.apellido_nombre} }
  end

  # GET /cartas
  # GET /cartas.json
  def index
    unless params[:prospecto].blank?
      if params[:commit] == "X"
        redirect_to cartas_path
      end
    end
    if !params[:prospecto].blank?
      @cartas = Carta.where(prospecto_id: params[:prospecto]).paginate(:page => params[:page], :per_page => 10).order('id ASC')
    else
      @cartas = Carta.paginate(:page => params[:page], :per_page => 10).order('id ASC')
    end
    @cantidad = @cartas.count
    @prospecto = Prospecto.all.order('apellido ASC')
  end

  # GET /cartas/1
  # GET /cartas/1.json
  def show
  end

  # GET /cartas/new
  def new
    @carta = Carta.new
  end

  # GET /cartas/1/edit
  def edit
  end

  def upload
    @carta = Carta.find(params[:id])
  end

  def upload_file
    if !params[:url_adjunto].blank?
      carta = Carta.find(params[:id])
      archivoIO = params[:url_adjunto]
      File.open(Rails.root.join('public', 'uploads', 'res',archivoIO.original_filename), 'wb') do |file|
        file.write(archivoIO.read)
      end
      carta.url_adjunto = 'uploads/res/' + archivoIO.original_filename.to_s
      carta.save
      redirect_to cartas_url
    else
      redirect_to action: 'upload', id: params[:id] #PONER PARAMETRO PARA VALIDACION DE URL_ADJUNTO VACIO
    end
  end

  # POST /cartas
  # POST /cartas.json
  def create
    @carta = Carta.new(carta_params)
    if !params[:carta][:url_adjunto].blank?
      archivoIO = params[:carta][:url_adjunto]
      File.open(Rails.root.join('public', 'uploads', 'res',archivoIO.original_filename), 'wb') do |file|
        file.write(archivoIO.read)
      end
      @carta.url_adjunto = 'uploads/res/' + archivoIO.original_filename.to_s
      #carta.save
      #redirect_to cartas_url
    end
    if !params[:carta][:cuerpo].blank?
      archivoIO = params[:carta][:cuerpo]
      File.open(Rails.root.join('public', 'uploads', 'carta',archivoIO.original_filename), 'wb') do |file|
        file.write(archivoIO.read)
      end
      @carta.cuerpo = 'uploads/carta/' + archivoIO.original_filename.to_s
    end

    respond_to do |format|
      if @carta.save
        unless params["prosp"].blank?
          format.html { redirect_to carta_path(@carta,prospecto: params["prosp"]), notice: 'Lettera è stato creato con successo.' }
        else
          format.html { redirect_to @carta, notice: 'Lettera è stato creato con successo.' }
        end
        format.json { render action: 'show', status: :created, location: @carta }
      else
        format.html { render action: 'new' }
        format.json { render json: @carta.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /cartas/1
  # PATCH/PUT /cartas/1.json
  def update
    if !params[:carta][:url_adjunto].blank?
      archivoIO = params[:carta][:url_adjunto]
      File.open(Rails.root.join('public', 'uploads', 'res',archivoIO.original_filename), 'wb') do |file|
        file.write(archivoIO.read)
      end
      #@carta.url_adjunto = 'uploads/res/' + archivoIO.original_filename.to_s
      params[:carta][:url_adjunto] = 'uploads/res/' + archivoIO.original_filename.to_s
      #carta.save
      #redirect_to cartas_url
    end
    if !params[:carta][:cuerpo].blank?
      archivoIO = params[:carta][:cuerpo]
      File.open(Rails.root.join('public', 'uploads', 'carta',archivoIO.original_filename), 'wb') do |file|
        file.write(archivoIO.read)
      end
      #@carta.cuerpo = 'uploads/carta/' + archivoIO.original_filename.to_s
      params[:carta][:cuerpo] = 'uploads/carta/' + archivoIO.original_filename.to_s
    end
    respond_to do |format|
      if @carta.update(carta_params)
        unless params["prosp"].blank?
          format.html { redirect_to carta_path(@carta,prospecto: params["prosp"]), notice: 'Lettera è stato aggiornato con successo.' }
          format.json { head :no_content }
        else
          format.html { redirect_to @carta, notice: 'Lettera è stato aggiornato con successo.' }
          format.json { head :no_content }
        end
      else
        format.html { render action: 'edit' }
        format.json { render json: @carta.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /cartas/1
  # DELETE /cartas/1.json
  def destroy
    @carta.destroy
    respond_to do |format|
      unless params[:prospecto].blank?
        format.html { redirect_to cartas_url(prospecto: params[:prospecto]) }
        format.json { head :no_content }
      else
        format.html { redirect_to cartas_url }
        format.json { head :no_content }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_carta
      @carta = Carta.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def carta_params
      params.require(:carta).permit(:usuario_id, :prospecto_id, :fecha, :cuerpo, :respuesta, :url_adjunto)
    end
end
