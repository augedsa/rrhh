class EntrenamientosController < ApplicationController
  before_action :set_entrenamiento, only: [:show, :edit, :update, :destroy]

  # GET /entrenamientos
  # GET /entrenamientos.json
  def index
    @entrenamientos = Entrenamiento.paginate(:page => params[:page], :per_page => 10).order('nombre ASC')
    @cantidad = Entrenamiento.count
  end

  # GET /entrenamientos/1
  # GET /entrenamientos/1.json
  def show
  end

  # GET /entrenamientos/new
  def new
    @entrenamiento = Entrenamiento.new
  end

  # GET /entrenamientos/1/edit
  def edit
  end

  # POST /entrenamientos
  # POST /entrenamientos.json
  def create
    @entrenamiento = Entrenamiento.new(entrenamiento_params)

    respond_to do |format|
      if @entrenamiento.save
        format.html { redirect_to @entrenamiento, notice: 'Livello di addestramento è stato creato con successo.' }
        format.json { render action: 'show', status: :created, location: @entrenamiento }
      else
        format.html { render action: 'new' }
        format.json { render json: @entrenamiento.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /entrenamientos/1
  # PATCH/PUT /entrenamientos/1.json
  def update
    respond_to do |format|
      if @entrenamiento.update(entrenamiento_params)
        format.html { redirect_to @entrenamiento, notice: 'Livello di addestramento è stato aggiornato con successo.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @entrenamiento.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /entrenamientos/1
  # DELETE /entrenamientos/1.json
  def destroy
    @entrenamiento.destroy
    respond_to do |format|
      format.html { redirect_to entrenamientos_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_entrenamiento
      @entrenamiento = Entrenamiento.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def entrenamiento_params
      params.require(:entrenamiento).permit(:nombre)
    end
end
