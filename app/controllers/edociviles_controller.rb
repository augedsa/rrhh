class EdocivilesController < ApplicationController
  before_action :set_edocivil, only: [:show, :edit, :update, :destroy]

  # GET /edociviles
  # GET /edociviles.json
  def index
    @edociviles = Edocivil.paginate(:page => params[:page], :per_page => 10).order('nombre ASC')
    @cantidad = Edocivil.count
  end

  # GET /edociviles/1
  # GET /edociviles/1.json
  def show
  end

  # GET /edociviles/new
  def new
    @edocivil = Edocivil.new
  end

  # GET /edociviles/1/edit
  def edit
  end

  # POST /edociviles
  # POST /edociviles.json
  def create
    @edocivil = Edocivil.new(edocivil_params)

    respond_to do |format|
      if @edocivil.save
        format.html { redirect_to @edocivil, notice: 'Stato civile è stato creato con successo.' }
        format.json { render action: 'show', status: :created, location: @edocivil }
      else
        format.html { render action: 'new' }
        format.json { render json: @edocivil.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /edociviles/1
  # PATCH/PUT /edociviles/1.json
  def update
    respond_to do |format|
      if @edocivil.update(edocivil_params)
        format.html { redirect_to @edocivil, notice: 'Stato civile è stato aggiornato con successo.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @edocivil.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /edociviles/1
  # DELETE /edociviles/1.json
  def destroy
    @edocivil.destroy
    respond_to do |format|
      format.html { redirect_to edociviles_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_edocivil
      @edocivil = Edocivil.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def edocivil_params
      params.require(:edocivil).permit(:nombre)
    end
end
