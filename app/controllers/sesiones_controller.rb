class SesionesController < ApplicationController
	skip_before_action :authorize
  layout "login"
  def new

  end

  def create
  	usuario = Usuario.find_by(nombre_usuario: params[:nombre_usuario])
  	if usuario and usuario.authenticate(params[:password])
      session[:nombre_usuario] = usuario.nombre_usuario
      session[:nombre_completo] = usuario.nombre_completo
      session[:usuario_id] = usuario.id
      session[:rol_id] = usuario.rol_id
      session[:rol_name] = Rol.find_by(id: usuario.rol_id).nombre
      session[:controller_act] = ""
      session[:expires_at] = 15.minutes.from_now
      puts "Parametro usr_id = " + params[:usr_id].to_s
      puts "Parametro controller_act = " + params[:controller_act].to_s
      puts "Session Usuario ID = " + session[:usuario_id].to_s
      if params[:usr_id] == session[:usuario_id].to_s and !params[:controller_act].blank?
        session[:controller_act] = params[:controller_act]
        redirect_to controller: eval("#{params[:controller_act]}_path")
      else
        redirect_to admin_url
      end
  	else
      unless  params[:usuario_id].blank? and params[:controller_act].blank?
  		  redirect_to login_url(controller_act: params[:controller_act], usr_id: params[:usr_id]), alert: t(".combinacion")
  	  else
        redirect_to login_url, alert: t(".combinacion")
      end
    end
  end

  def destroy
  	session[:nombre_usuario] = nil
    session[:nombre_completo] = nil
    session[:usuario_id] = nil
    session[:rol_id] = nil
    session[:rol_name] = nil
    session[:expires_at] = nil
    session[:controller_act] = nil
	  redirect_to login_url, notice: "Scollegato"
  end
end
