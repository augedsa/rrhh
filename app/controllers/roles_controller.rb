class RolesController < ApplicationController
  before_action :set_rol, only: [:show, :edit, :update, :destroy]

  # GET /roles
  # GET /roles.json
  def index
    @roles = Rol.paginate(:page => params[:page], :per_page => 10).order('nombre ASC')
    @cantidad = @roles.count
  end

  # GET /roles/1
  # GET /roles/1.json
  def show
  end

  # GET /roles/new
  def new
    @rol = Rol.new
  end

  # GET /roles/1/edit
  def edit
  end

  def permisologia
  end

  def updatePermisologia
    permisologiaMenus = PermisologiaMenu.all
    permisologiaMenus.each do |permisologiaMenu|
      if params["PM"+permisologiaMenu.id.to_s]
        permisologiaMenu.permiso = "true"
      else
        permisologiaMenu.permiso = "false"
      end
      if params["PMN"+permisologiaMenu.id.to_s]
        permisologiaMenu.permiso_new = "true"
      else
        permisologiaMenu.permiso_new = "false"
      end
      if params["PME"+permisologiaMenu.id.to_s]
        permisologiaMenu.permiso_edit = "true"
      else
        permisologiaMenu.permiso_edit = "false"
      end
      if params["PMD"+permisologiaMenu.id.to_s]
        permisologiaMenu.permiso_delete = "true"
      else
        permisologiaMenu.permiso_delete = "false"
      end
      permisologiaMenu.save
    end
    permisologiaConfiguraciones = PermisologiaConfiguracion.all
    permisologiaConfiguraciones.each do |permisologiaConfiguracion|
      if params["PC"+permisologiaConfiguracion.id.to_s]
        permisologiaConfiguracion.permiso = "true"
      else
        permisologiaConfiguracion.permiso = "false"
      end
      if params["PCN"+permisologiaConfiguracion.id.to_s]
        permisologiaConfiguracion.permiso_new = "true"
      else
        permisologiaConfiguracion.permiso_new = "false"
      end
      if params["PCE"+permisologiaConfiguracion.id.to_s]
        permisologiaConfiguracion.permiso_edit = "true"
      else
        permisologiaConfiguracion.permiso_edit = "false"
      end
      if params["PCD"+permisologiaConfiguracion.id.to_s]
        permisologiaConfiguracion.permiso_delete = "true"
      else
        permisologiaConfiguracion.permiso_delete = "false"
      end
      permisologiaConfiguracion.save
    end
    respond_to do |format|
      format.html {redirect_to permisologia_path, notice: 'I autorizzazioni sono stati aggiornati' }
    end

  end

  # POST /roles
  # POST /roles.json
  def create
    @rol = Rol.new(rol_params)

    respond_to do |format|
      if @rol.save
        menus = Menu.all
        menus.each do |menu|
          permisologiaMenus = PermisologiaMenu.where(rol_id: @rol.id).where(menu_id: menu.id)
          if permisologiaMenus.count == 0
            #Insert new funcs.
            permisologiaMenu = PermisologiaMenu.new
            permisologiaMenu.rol_id = @rol.id
            permisologiaMenu.menu_id = menu.id
            permisologiaMenu.permiso = false
            permisologiaMenu.permiso_new = false
            permisologiaMenu.permiso_edit = false
            permisologiaMenu.permiso_delete = false
            permisologiaMenu.save
          end
        end
        configuraciones = Configuracion.all
        configuraciones.each do |configuracion|
        permisologiaConfiguraciones = PermisologiaConfiguracion.where(rol_id: @rol.id).where(configuracion_id: configuracion.id)
            if permisologiaConfiguraciones.count == 0
            #Insert new funcs.
            permisologiaConfiguracion= PermisologiaConfiguracion.new
            permisologiaConfiguracion.rol_id = @rol.id
            permisologiaConfiguracion.configuracion_id = configuracion.id
            permisologiaConfiguracion.permiso = false
            permisologiaConfiguracion.permiso_new = false
            permisologiaConfiguracion.permiso_edit = false
            permisologiaConfiguracion.permiso_delete = false
            permisologiaConfiguracion.save
          end
        end
        format.html { redirect_to permisologia_path, notice: 'Ruolo è stato creato con successo.' }
        format.json { render action: 'show', status: :created, location: @rol }
      else
        format.html { render action: 'new' }
        format.json { render json: @rol.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /roles/1
  # PATCH/PUT /roles/1.json
  def update
    respond_to do |format|
      if @rol.update(rol_params)
        format.html { redirect_to permisologia_path, notice: 'Ruolo è stato aggiornato con successo.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @rol.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /roles/1
  # DELETE /roles/1.json
  def destroy
    @rol.destroy
    respond_to do |format|
      format.html { redirect_to roles_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_rol
      @rol = Rol.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def rol_params
      params.require(:rol).permit(:nombre)
    end
end
