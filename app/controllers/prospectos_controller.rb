class ProspectosController < ApplicationController
  before_action :set_prospecto, only: [:show, :edit, :update, :destroy]

  # autocomplete :prospecto, :apellido,:extra_data => [:id]
  def autocomplete_prospecto_apellido
    term = params[:term].upcase
    prospectos = Prospecto.where("upper(apellido) LIKE ? or upper(nombre) LIKE ? or upper(concat(apellido,' ', nombre)) like ?","%#{term}%","%#{term}%","%#{term}%").order(:apellido)
    #prospectos = Prospecto.where("apellido LIKE ?","%#{term}%").order(:apellido)
    render :json => prospectos.map { |prospecto| {:id => prospecto.id, :label => prospecto.apellido_nombre, :value => prospecto.apellido_nombre} }
  end

  # GET /prospectos
  # GET /prospectos.json
  def index
    @prospectos = Prospecto.all
    if !params[:prospecto].blank?
      @prospectos = @prospectos.where(id: params[:prospecto])
    else
      if !params[:area].blank?
        @prospectos = @prospectos.where(area_id: params[:area])
      end
      if !params[:organizacion].blank?
        @prospectos = @prospectos.where(organizacion_id: params[:organizacion])
      end
      if !params[:habilidad].blank?
        @prospectos = @prospectos.where(habilidad_id: params[:habilidad])
      end
      if !params[:entrenamiento].blank?
        @prospectos = @prospectos.where(entrenamiento_id: params[:entrenamiento])
      end
      if !params[:tipologia].blank?
        @prospectos = @prospectos.where(tipologia_id: params[:tipologia])
      end
      if !params[:tipo].blank?
        @prospectos = @prospectos.where(tipo_id: params[:tipo])
      end
      if !params[:edocivil].blank?
        @prospectos = @prospectos.where(edocivil_id: params[:edocivil])
      end
    end
    @prospectos = @prospectos.paginate(:page => params[:page], :per_page => 10).order('nombre ASC')
    @cantidad = @prospectos.count
    @area = Area.all
    @organizacion = Organizacion.all
    @habilidad = Habilidad.all
    @entrenamiento = Entrenamiento.all
    @tipologia = Tipologia.all
    @tipo = Tipo.all
    @edocivil = Edocivil.all

    respond_to do |format|
      format.html do
        unless params[:prospecto].blank?
          if params[:commit] == "X"
            redirect_to prospectos_path
          end
        end
      end
      format.pdf do
        pdf = PdfGenerator.new
        pdf.list_prospecto(@prospectos, @cantidad)
        send_data pdf.render, filename: "prospecto_list.pdf",type: "application/pdf",disposition: "inline"
      end
    end

  end

  def filtrar_prospectos
        @prospectos = Prospecto.joins(:area).where(area_id: params[:area_id])
  end

  def simple

  end

  # GET /prospectos/1
  # GET /prospectos/1.json
  def show
    respond_to do |format|
      format.html
      format.pdf do
        prospecto = Prospecto.find(params[:id])
        pdf = PdfGenerator.new
        pdf.show_prospecto(prospecto)
        send_data pdf.render, filename: "prospecto_#{params[:id]}.pdf",type: "application/pdf",disposition: "inline"
      end
    end
  end

  # GET /prospectos/new
  def new
    @prospecto = Prospecto.new
  end

  # GET /prospectos/1/edit
  def edit
  end

  # POST /prospectos
  # POST /prospectos.json
  def create
    @prospecto = Prospecto.new(prospecto_params)

    respond_to do |format|
      if @prospecto.save
        format.html { redirect_to @prospecto, notice: "Il Prospetto: #{@prospecto.apellido_nombre}  è stato creato con successo." }
        format.json { render action: 'show', status: :created, location: @prospecto }
      else
        format.html { render action: 'new' }
        format.json { render json: @prospecto.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /prospectos/1
  # PATCH/PUT /prospectos/1.json
  def update
    respond_to do |format|
      if @prospecto.update(prospecto_params)
        format.html { redirect_to @prospecto, notice: "Il Prospetto: #{@prospecto.apellido_nombre} è stato aggiornato con successo." }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @prospecto.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /prospectos/1
  # DELETE /prospectos/1.json
  def destroy

    @prospecto.destroy
    respond_to do |format|
      format.html { redirect_to prospectos_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_prospecto
      @prospecto = Prospecto.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def prospecto_params
      params.require(:prospecto).permit(:nombre, :direccion, :telefono, :correo, :habilidad_id, :entrenamiento_id, :organizacion_id, :tipo_id, :edad, :edocivil_id, :hijos, :deudas, :area_id, :apellido, :ciudad_id, :zona_postal, :tipologia_id, :public, :organizacionpublic_id, :usuario_id)
    end

end
