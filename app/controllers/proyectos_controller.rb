class ProyectosController < ApplicationController
  before_action :set_proyecto, only: [:show, :edit, :update, :destroy]

  # autocomplete :prospecto, :apellido,:extra_data => [:id]
  def autocomplete_prospecto_apellido
    term = params[:term].upcase
    prospectos = Prospecto.where("upper(apellido) LIKE ? or upper(nombre) LIKE ? or upper(concat(apellido,' ', nombre)) like ?","%#{term}%","%#{term}%","%#{term}%").order(:apellido)
    #prospectos = Prospecto.where("apellido LIKE ?","%#{term}%").order(:apellido)
    render :json => prospectos.map { |prospecto| {:id => prospecto.id, :label => prospecto.apellido_nombre, :value => prospecto.apellido_nombre} }
  end

  # autocomplete :prospecto, :apellido,:extra_data => [:id]
  def autocomplete_usuario
    term = params[:term].upcase
    usuarios = Usuario.where("upper(nombre_usuario) like ?","%#{term}%").order(:nombre_usuario)
    render :json => usuarios.map { |usuario| {:id => usuario.id, :label => usuario.nombre_usuario, :value => usuario.nombre_usuario} }
  end

  # GET /proyectos
  # GET /proyectos.json
  def index
    if !params[:prospecto].blank? or !params[:usuario].blank?
      if params[:commit] == "X"
        redirect_to proyectos_path
      end
    end
    @proyectos = Proyecto.all
    if !params[:usuario].blank?
      @proyectos = @proyectos.where(usuario_id: params[:usuario])
    end
    if !params[:prospecto].blank?
      @proyectos = @proyectos.where(prospecto_id: params[:prospecto])
    end
    @proyectos = @proyectos.paginate(:page => params[:page], :per_page => 10).order('nombre ASC')
    @cantidad = @proyectos.count
    @usuario = Usuario.all.order('nombre_usuario ASC')
    @prospecto = Prospecto.all.order('apellido ASC')
  end

  # GET /proyectos/1
  # GET /proyectos/1.json
  def show
  end

  # GET /proyectos/new
  def new
  @proyecto = Proyecto.new
  end

  # GET /proyectos/1/edit
  def edit
  end

  # POST /proyectos
  # POST /proyectos.json
  def create
    @proyecto = Proyecto.new(proyecto_params)
    respond_to do |format|
      proyectoId = ""
      unless @proyecto.prospecto.blank?
        proyectoId = @proyecto.prospecto.id
      end
      if Proyecto.find_by(prospecto_id: proyectoId)
        unless params["prosp"].blank?
          format.html { redirect_to proyectos_path(prospecto: params["prosp"]), alert: 'El prospecto ha già un progetto assegnato' }
        else
          format.html { redirect_to proyectos_path, alert: 'El prospecto ha già un progetto assegnato' }
        end
      elsif @proyecto.save
        unless params["prosp"].blank?
          format.html { redirect_to proyecto_path(@proyecto, prospecto: params["prosp"]), notice: 'Progetto è stato creato con successo.' }
        else
          format.html { redirect_to @proyecto, notice: 'Progetto è stato creato con successo.' }
        end
        format.json { render action: 'show', status: :created, location: @proyecto }
      else
        format.html { render action: 'new' }
        format.json { render json: @proyecto.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /proyectos/1
  # PATCH/PUT /proyectos/1.json
  def update
    respond_to do |format|
      if @proyecto.update(proyecto_params)
        unless params["prosp"].blank?
          format.html { redirect_to proyecto_path(@proyecto, prospecto: params["prosp"]), notice: 'Progetto è stato aggiornato con successo.' }
        else
          format.html { redirect_to @proyecto, notice: 'Progetto è stato aggiornato con successo.' }
        end
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @proyecto.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /proyectos/1
  # DELETE /proyectos/1.json
  def destroy
    #destroy objetivos donde esten contenidos los @proyectos
    @proyecto.destroy
    respond_to do |format|
      unless params[:prospecto].blank?
        format.html { redirect_to proyectos_url(prospecto: params[:prospecto]) }
        format.json { head :no_content }
      else
        format.html { redirect_to proyectos_url }
        format.json { head :no_content }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_proyecto
      @proyecto = Proyecto.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def proyecto_params
      params.require(:proyecto).permit(:nombre, :prospecto_id, :usuario_id)
    end


end
