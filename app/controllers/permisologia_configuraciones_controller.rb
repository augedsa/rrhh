class PermisologiaConfiguracionesController < ApplicationController
  before_action :set_permisologia_configuracion, only: [:show, :edit, :update, :destroy]

  # GET /permisologia_configuraciones
  # GET /permisologia_configuraciones.json
  def index
    @permisologia_configuraciones = PermisologiaConfiguracion.all
  end

  # GET /permisologia_configuraciones/1
  # GET /permisologia_configuraciones/1.json
  def show
  end

  # GET /permisologia_configuraciones/new
  def new
    @permisologia_configuracion = PermisologiaConfiguracion.new
  end

  # GET /permisologia_configuraciones/1/edit
  def edit
  end

  # POST /permisologia_configuraciones
  # POST /permisologia_configuraciones.json
  def create
    @permisologia_configuracion = PermisologiaConfiguracion.new(permisologia_configuracion_params)

    respond_to do |format|
      if @permisologia_configuracion.save
        format.html { redirect_to @permisologia_configuracion, notice: 'Permisologia configuracion was successfully created.' }
        format.json { render action: 'show', status: :created, location: @permisologia_configuracion }
      else
        format.html { render action: 'new' }
        format.json { render json: @permisologia_configuracion.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /permisologia_configuraciones/1
  # PATCH/PUT /permisologia_configuraciones/1.json
  def update
    respond_to do |format|
      if @permisologia_configuracion.update(permisologia_configuracion_params)
        format.html { redirect_to @permisologia_configuracion, notice: 'Permisologia configuracion was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @permisologia_configuracion.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /permisologia_configuraciones/1
  # DELETE /permisologia_configuraciones/1.json
  def destroy
    @permisologia_configuracion.destroy
    respond_to do |format|
      format.html { redirect_to permisologia_configuraciones_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_permisologia_configuracion
      @permisologia_configuracion = PermisologiaConfiguracion.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def permisologia_configuracion_params
      params.require(:permisologia_configuracion).permit(:rol_id, :configuracion_id, :permiso)
    end
end
