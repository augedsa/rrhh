class InicioController < ApplicationController
  skip_before_filter :authorize
  layout "login"

 def olvido_clave
	@clave_plana_aleatoria = (rand * 9999999).ceil # genera la clave aleatoria
	@usuario_encontrado = Usuario.find_by_correo(params[:correo])
	if !@usuario_encontrado.nil?
		@usuario_encontrado.password_digest = Usuario.encriptar(@clave_plana_aleatoria)
		@usuario_encontrado.save
		ClaveMailer.olvido_clave(@usuario_encontrado,@clave_plana_aleatoria).deliver
		redirect_to login_url, alert: "Su nueva clave de acceso al Sistema ha sido enviada a su correo"
	else
		#redirect_to inicio_url, alert: "El correo ingresado no existe"

	end
 end

# def asignar_clave
#	@usuarios = Usuario.all
#  	@usuarios.each do |usuario|

#		@clave_plana_aleatoria = (rand * 9999999).ceil # genera la clave aleatoria
#		

#		usuario.password_digest = Usuario.encriptar(@clave_plana_aleatoria)
#		@usuario_encontrado.save
#		ClaveMailer.asignar_clave(@usuario_encontrado,@clave_plana_aleatoria).deliver
#		redirect_to ppal_admin_enviar_clave_url, :notice => "Se han enviado las clave de acceso al Sistema a los correos de los estudiantes"
#	   
#  	end
# end

end
