class ObjetivosController < ApplicationController
  before_action :set_objetivo, only: [:show, :edit, :update, :destroy]

  # GET /objetivos
  # GET /objetivos.json
  def index
    @objetivos = Objetivo.paginate(:page => params[:page], :per_page => 10).order('id ASC')
    @cantidad = @objetivos.count
  end

  # GET /objetivos/1
  # GET /objetivos/1.json
  def show
  end

  # GET /objetivos/new
  def new
    @objetivo = Objetivo.new
  end

  # GET /objetivos/1/edit
  def edit
  end

  def obj_proyecto
    if params[:proyecto_id].blank?
      redirect_to objetivos_url
    end
    @objetivos = Objetivo.where(proyecto_id: params[:proyecto_id]).paginate(:page => params[:page], :per_page => 10).order('id ASC')
    @cantidad = @objetivos.count
    @proy = Proyecto.find_by(id: params[:proyecto_id])
    @prospID = @proy.prospecto.id
  end

  # POST /objetivos
  # POST /objetivos.json
  def create
    @objetivo = Objetivo.new(objetivo_params)
    respond_to do |format|
      if @objetivo.save
          unless params["proy_id"].blank?
            format.html { redirect_to objetivo_path(@objetivo, proyecto_id: @objetivo.proyecto_id), notice: 'Obiettivo è stato creato con successo.' }
          else
            format.html { redirect_to @objetivo, notice: 'Obiettivo è stato creato con successo.' }
          end
          format.json { render action: 'show', status: :created, location: @objetivo }
      else
        format.html { render action: 'new' }
        format.json { render json: @objetivo.errors, status: :unprocessable_entity }
      end
    end
  end

    # PATCH/PUT /objetivos/1
    # PATCH/PUT /objetivos/1.json
  def update
    respond_to do |format|
      if @objetivo.update(objetivo_params)
          unless params["proy_id"].blank?
            format.html { redirect_to objetivo_path(@objetivo, proyecto_id: @objetivo.proyecto_id), notice: 'Obiettivo è stato aggiornato con successo.' }
          else
            format.html { redirect_to @objetivo, notice: 'Obiettivo è stato aggiornato con successo.' }
          end
          format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @objetivo.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /objetivos/1
  # DELETE /objetivos/1.json
  def destroy
    @objetivo.destroy
    respond_to do |format|
      if !params[:proyecto_id].blank?
        format.html { redirect_to action: 'obj_proyecto', proyecto_id: params[:proyecto_id] }
        format.json { head :no_content }
      else
        format.html { redirect_to objetivos_url }
        format.json { head :no_content }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_objetivo
      @objetivo = Objetivo.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def objetivo_params
      params.require(:objetivo).permit(:titulo, :cuerpo, :fecha_inicio, :fecha_fin, :estado_id, :proyecto_id)
    end
end
