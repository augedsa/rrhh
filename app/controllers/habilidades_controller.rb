class HabilidadesController < ApplicationController
  before_action :set_habilidad, only: [:show, :edit, :update, :destroy]

  # GET /habilidades
  # GET /habilidades.json
  def index
    @habilidades = Habilidad.paginate(:page => params[:page], :per_page => 10).order('nombre ASC')
    @cantidad = Habilidad.count
  end

  # GET /habilidades/1
  # GET /habilidades/1.json
  def show
  end

  # GET /habilidades/new
  def new
    @habilidad = Habilidad.new
  end

  # GET /habilidades/1/edit
  def edit
  end

  # POST /habilidades
  # POST /habilidades.json
  def create
    @habilidad = Habilidad.new(habilidad_params)

    respond_to do |format|
      if @habilidad.save
        format.html { redirect_to @habilidad, notice: 'Livello di caso è stato creato con successo.' }
        format.json { render action: 'show', status: :created, location: @habilidad }
      else
        format.html { render action: 'new' }
        format.json { render json: @habilidad.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /habilidades/1
  # PATCH/PUT /habilidades/1.json
  def update
    respond_to do |format|
      if @habilidad.update(habilidad_params)
        format.html { redirect_to @habilidad, notice: 'Livello di caso è stato aggiornato con successo.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @habilidad.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /habilidades/1
  # DELETE /habilidades/1.json
  def destroy
    @habilidad.destroy
    respond_to do |format|
      format.html { redirect_to habilidades_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_habilidad
      @habilidad = Habilidad.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def habilidad_params
      params.require(:habilidad).permit(:nombre)
    end
end
