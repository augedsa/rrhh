class PermisologiasMenuController < ApplicationController
  before_action :set_permisologia_menu, only: [:show, :edit, :update, :destroy]

  # GET /permisologias_menu
  # GET /permisologias_menu.json
  def index
    @permisologias_menu = PermisologiaMenu.all
  end

  # GET /permisologias_menu/1
  # GET /permisologias_menu/1.json
  def show
  end

  # GET /permisologias_menu/new
  def new
    @permisologia_menu = PermisologiaMenu.new
  end

  # GET /permisologias_menu/1/edit
  def edit
  end

  # POST /permisologias_menu
  # POST /permisologias_menu.json
  def create
    @permisologia_menu = PermisologiaMenu.new(permisologia_menu_params)

    respond_to do |format|
      if @permisologia_menu.save
        format.html { redirect_to @permisologia_menu, notice: 'Permisologia menu was successfully created.' }
        format.json { render action: 'show', status: :created, location: @permisologia_menu }
      else
        format.html { render action: 'new' }
        format.json { render json: @permisologia_menu.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /permisologias_menu/1
  # PATCH/PUT /permisologias_menu/1.json
  def update
    respond_to do |format|
      if @permisologia_menu.update(permisologia_menu_params)
        format.html { redirect_to @permisologia_menu, notice: 'Permisologia menu was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @permisologia_menu.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /permisologias_menu/1
  # DELETE /permisologias_menu/1.json
  def destroy
    @permisologia_menu.destroy
    respond_to do |format|
      format.html { redirect_to permisologias_menu_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_permisologia_menu
      @permisologia_menu = PermisologiaMenu.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def permisologia_menu_params
      params.require(:permisologia_menu).permit(:rol_id, :menu_id, :permiso)
    end
end
