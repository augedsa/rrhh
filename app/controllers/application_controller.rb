class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_action :set_i18n_locale_from_params
  before_action :session_expires
  before_action :authorize

   def set_i18n_locale_from_params
    if params[:locale]
      if I18n.available_locales.map(&:to_s).include?(params[:locale])
        I18n.locale = params[:locale]
      else
        flash.now[:notice] =
          "#{params[:locale]} Traduzione non disponibile"
        logger.error flash.now[:notice]
      end
    end
  end

  def default_url_options
    { locale: I18n.locale }
  end

  def session_expires
  	if !session[:usuario_id].blank?
  	  expire_time = session[:expires_at] || Time.now
      @time_left = (expire_time - Time.now).to_i
  	  update_session_time
      if !(@time_left > 0)
        usr = session[:usuario_id]
        session_manual_reset #reset_session
        #redirect_to login_url(controller_act: session[:controller_act], usr_id: usr), alert: "La sessione è scaduta. Per Favore Accedi di nuovo"
        redirect_to login_url, alert: "La sessione è scaduta. Per Favore Accedi di nuovo"
      elsif params[:controller] == "sesiones"
        if params[:action] != "destroy"
          redirect_to admin_path
        end
      end
  	end
  end

  def session_manual_reset
   session[:nombre_usuario] = nil
   session[:nombre_completo] = nil
   session[:usuario_id] = nil
	 session[:rol_id] = nil
   session[:rol_name] = nil
   session[:expires_at] = nil
  end


  def update_session_time
    session[:expires_at] = 15.minutes.from_now
  end

	protected

	 def authorize
    unless Usuario.find_by(id: session[:usuario_id])
      redirect_to login_url, notice: "Per favore In Esci"
    else
      session[:controller_act] = params[:controller]
      if session[:rol_id] != 1 and  params[:controller] == "roles" and params[:action] == "permisologias"
        session[:controller_act] = ""
        redirect_to admin_path
      end
      # Menu Funcs
        controlador = params[:controller]
        if controlador == "objetivos"
          controlador = "proyectos"
        end
        if menu_p = Menu.find_by(controlador_menu: controlador)
          if permisologiaMenu = PermisologiaMenu.where("rol_id = ? AND menu_id = ?", session[:rol_id], menu_p.id).first
            if permisologiaMenu.permiso
              if (params[:action] == "new" and !permisologiaMenu.permiso_new) or (params[:action] == "edit" and !permisologiaMenu.permiso_edit) or (params[:action] == "destroy" and !permisologiaMenu.permiso_delete)
                redirect_to eval("#{menu_p.path_menu}")
              end
              params[:permiso_new] = permisologiaMenu.permiso_new
              params[:permiso_edit] = permisologiaMenu.permiso_edit
              params[:permiso_delete] = permisologiaMenu.permiso_delete
            else
              session[:controller_act] = ""
              redirect_to admin_path
            end
          end
        end
      # Config Funcs
        if config_p = Configuracion.find_by(controlador_configuracion: params[:controller])
          if permisologiaConfig = PermisologiaConfiguracion.where("rol_id = ? AND configuracion_id = ?", session[:rol_id], config_p.id).first
            if permisologiaConfig.permiso
              if (params[:action] == "new" and !permisologiaConfig.permiso_new) or (params[:action] == "edit" and !permisologiaConfig.permiso_edit) or (params[:action] == "destroy" and !permisologiaConfig.permiso_delete)
                redirect_to eval("#{config_p.path_configuracion}")
              end
              params[:permiso_new] = permisologiaConfig.permiso_new
              params[:permiso_edit] = permisologiaConfig.permiso_edit
              params[:permiso_delete] = permisologiaConfig.permiso_delete
            else
              session[:controller_act] = ""
              redirect_to admin_path
            end
          end
        end
      # Other Funcs
      end
	 end
end
