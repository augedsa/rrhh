class TipologiasController < ApplicationController
  before_action :set_tipologia, only: [:show, :edit, :update, :destroy]

  # GET /tipologias
  # GET /tipologias.json
  def index
    @tipologias = Tipologia.paginate(:page => params[:page], :per_page => 10).order('nombre ASC')
    @cantidad = Tipologia.count
  end

  # GET /tipologias/1
  # GET /tipologias/1.json
  def show
  end

  # GET /tipologias/new
  def new
    @tipologia = Tipologia.new
  end

  # GET /tipologias/1/edit
  def edit
  end

  # POST /tipologias
  # POST /tipologias.json
  def create
    @tipologia = Tipologia.new(tipologia_params)

    respond_to do |format|
      if @tipologia.save
        format.html { redirect_to @tipologia, notice: 'Tipologia è stato creato con successo.' }
        format.json { render action: 'show', status: :created, location: @tipologia }
      else
        format.html { render action: 'new' }
        format.json { render json: @tipologia.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /tipologias/1
  # PATCH/PUT /tipologias/1.json
  def update
    respond_to do |format|
      if @tipologia.update(tipologia_params)
        format.html { redirect_to @tipologia, notice: 'Tipologia è stato aggiornato con successo.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @tipologia.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /tipologias/1
  # DELETE /tipologias/1.json
  def destroy
    @tipologia.destroy
    respond_to do |format|
      format.html { redirect_to tipologias_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_tipologia
      @tipologia = Tipologia.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def tipologia_params
      params.require(:tipologia).permit(:nombre)
    end
end
