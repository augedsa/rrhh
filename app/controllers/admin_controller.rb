class AdminController < ApplicationController
  skip_before_action :authorize
  def config_func
  	roles = Rol.all
  	roles.each do |rol|
  		menus = Menu.all
  		menus.each do |menu|
			permisologiaMenus = PermisologiaMenu.where(rol_id: rol.id).where(menu_id: menu.id)
			if permisologiaMenus.count == 0
				#Insert new funcs.
				permisologiaMenu = PermisologiaMenu.new
				permisologiaMenu.rol_id = rol.id
				permisologiaMenu.menu_id = menu.id
				permisologiaMenu.permiso = "false"
				permisologiaMenu.permiso_new = "false"
				permisologiaMenu.permiso_edit = "false"
				permisologiaMenu.permiso_delete = "false"
				permisologiaMenu.save
			end
  		end
  		configuraciones = Configuracion.all
  		configuraciones.each do |configuracion|
		permisologiaConfiguraciones = PermisologiaConfiguracion.where(rol_id: rol.id).where(configuracion_id: configuracion.id)
			if permisologiaConfiguraciones.count == 0
				#Insert new funcs.
				permisologiaConfiguracion= PermisologiaConfiguracion.new
				permisologiaConfiguracion.rol_id = rol.id
				permisologiaConfiguracion.configuracion_id = configuracion.id
				permisologiaConfiguracion.permiso = "false"
				permisologiaConfiguracion.permiso_new = "false"
				permisologiaConfiguracion.permiso_edit = "false"
				permisologiaConfiguracion.permiso_delete = "false"
				permisologiaConfiguracion.save
			end
		end
  	end

  end

  def index
  end
end
