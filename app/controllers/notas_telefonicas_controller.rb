class NotasTelefonicasController < ApplicationController
  before_action :set_nota_telefonicas, only: [:show, :edit, :update, :destroy]

  # autocomplete :prospecto, :apellido,:extra_data => [:id]
  def autocomplete_prospecto_apellido
    term = params[:term].upcase
    prospectos = Prospecto.where("upper(apellido) LIKE ? or upper(nombre) LIKE ? or upper(concat(apellido,' ', nombre)) like ?","%#{term}%","%#{term}%","%#{term}%").order(:apellido)
    #prospectos = Prospecto.where("apellido LIKE ?","%#{term}%").order(:apellido)
    render :json => prospectos.map { |prospecto| {:id => prospecto.id, :label => prospecto.apellido_nombre, :value => prospecto.apellido_nombre} }
  end

  # GET /notas_telefonicas
  # GET /notas_telefonicas.json
  def index
    unless params[:prospecto].blank?
      if params[:commit] == "X"
        redirect_to notas_telefonicas_path
      end
    end
    if !params[:prospecto].blank?
      @notas_telefonicas = NotaTelefonicas.where(prospecto_id: params[:prospecto]).paginate(:page => params[:page], :per_page => 10).order('id ASC')
    else
      @notas_telefonicas = NotaTelefonicas.paginate(:page => params[:page], :per_page => 10).order('id ASC')
    end
    @cantidad = @notas_telefonicas.count
    #@prospecto = Prospecto.all
  end

  # GET /notas_telefonicas/1
  # GET /notas_telefonicas/1.json
  def show
    if !params[:cal].blank?
      if @nota_telefonicas.fecha_recordatorio.to_formatted_s(:db) <= Time.now.to_formatted_s(:db)
        @nota_telefonicas.visitado = true
        @nota_telefonicas.save
      end
    end
  end

  # GET /notas_telefonicas/new
  def new
    @nota_telefonicas = NotaTelefonicas.new
  end

  # GET /notas_telefonicas/1/edit
  def edit
  end

  # POST /notas_telefonicas
  # POST /notas_telefonicas.json
  def create
    @nota_telefonicas = NotaTelefonicas.new(nota_telefonicas_params)
    if @nota_telefonicas.recordatorio and @nota_telefonicas.fecha_recordatorio.to_formatted_s(:db) <= @nota_telefonicas.fecha.to_formatted_s(:db)
      @nota_telefonicas.errors.add(:fecha_recordatorio, "non può essere inferiore alla data corrente.")
      respond_to do |format|
        format.html { render action: 'new' }
        format.json { render json: @nota_telefonicas.errors, status: :unprocessable_entity }
      end
    else
      respond_to do |format|
        if @nota_telefonicas.save
          unless params["prosp"].blank?
            format.html { redirect_to nota_telefonicas_path(@nota_telefonicas, prospecto: params["prosp"]), notice: 'Nota telefonica  è stato creato con successo.' }
          else
            format.html { redirect_to @nota_telefonicas, notice: 'Nota telefonica  è stato creato con successo.' }
          end
          format.json { render action: 'show', status: :created, location: @nota_telefonicas }
        else
          format.html { render action: 'new' }
          format.json { render json: @nota_telefonicas.errors, status: :unprocessable_entity }
        end
      end
    end
  end

  # PATCH/PUT /notas_telefonicas/1
  # PATCH/PUT /notas_telefonicas/1.json
  def update
    fecha = Time.new(nota_telefonicas_params["fecha(1i)"],nota_telefonicas_params["fecha(2i)"],nota_telefonicas_params["fecha(3i)"],nota_telefonicas_params["fecha(4i)"],nota_telefonicas_params["fecha(5i)"])
    fecha_recordatorio = Time.new(nota_telefonicas_params["fecha_recordatorio(1i)"],nota_telefonicas_params["fecha_recordatorio(2i)"],nota_telefonicas_params["fecha_recordatorio(3i)"],nota_telefonicas_params["fecha_recordatorio(4i)"],nota_telefonicas_params["fecha_recordatorio(5i)"])
    if nota_telefonicas_params[:recordatorio] == "1" and fecha_recordatorio.to_formatted_s(:db) <= fecha.to_formatted_s(:db)
      @nota_telefonicas.errors.add(:fecha_recordatorio, "non può essere inferiore alla data corrente.")
      respond_to do |format|
        format.html { render action: 'edit' }
        format.json { render json: @nota_telefonicas.errors, status: :unprocessable_entity }
      end
    else
      respond_to do |format|
        if @nota_telefonicas.update(nota_telefonicas_params)
          unless params["prosp"].blank?
            format.html { redirect_to nota_telefonicas_path(@nota_telefonicas, prospecto: params["prosp"]), notice: 'Nota telefonica è stato aggiornato con successo.' }
          else
            format.html { redirect_to @nota_telefonicas, notice: 'Nota telefonica è stato aggiornato con successo.' }
          end
          #format.html { redirect_to @nota_telefonicas, notice: 'Nota telefonica è stato aggiornato con successo.' }
          format.json { head :no_content }
        else
          format.html { render action: 'edit' }
          format.json { render json: @nota_telefonicas.errors, status: :unprocessable_entity }
        end
      end
    end
  end

  # DELETE /notas_telefonicas/1
  # DELETE /notas_telefonicas/1.json
  def destroy
    @nota_telefonicas.destroy
    respond_to do |format|
      unless params[:prospecto].blank?
        format.html { redirect_to notas_telefonicas_url(prospecto: params[:prospecto]) }
        format.json { head :no_content }
      else
        format.html { redirect_to notas_telefonicas_url }
        format.json { head :no_content }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_nota_telefonicas
      @nota_telefonicas = NotaTelefonicas.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def nota_telefonicas_params
      params.require(:nota_telefonicas).permit(:usuario_id, :prospecto_id, :fecha, :nota_descripcion, :fecha_recordatorio, :recordatorio, :visitado)
    end
end
