json.array!(@usuarios) do |usuario|
  json.extract! usuario, :id, :nombre_usuario, :nombre_completo, :correo, :rol_id
  json.url usuario_url(usuario, format: :json)
end
