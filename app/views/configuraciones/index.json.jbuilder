json.array!(@configuraciones) do |configuracion|
  json.extract! configuracion, :id, :nombre_configuracion, :path_configuracion, :controlador_configuracion
  json.url configuracion_url(configuracion, format: :json)
end
