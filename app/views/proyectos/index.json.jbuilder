json.array!(@proyectos) do |proyecto|
  json.extract! proyecto, :id, :nombre, :prospecto_id, :usuario_id
  json.url proyecto_url(proyecto, format: :json)
end
