json.array!(@prospectos) do |prospecto|
  json.extract! prospecto, :id, :nombre, :direccion, :telefono, :correo, :habilidad_id, :entrenamiento_id, :staff, :exstaff, :organizacion_id, :tipo_id, :edad, :edocivil_id, :hijos, :deudas, :area_id
  json.url prospecto_url(prospecto, format: :json)
end
