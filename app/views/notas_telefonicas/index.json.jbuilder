json.array!(@notas_telefonicas) do |nota_telefonicas|
  json.extract! nota_telefonicas, :id, :usuario_id, :prospecto_id, :fecha, :nota_descripcion, :fecha_recordatorio
  json.url nota_telefonicas_url(nota_telefonicas, format: :json)
end
