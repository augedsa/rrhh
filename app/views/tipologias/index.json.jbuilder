json.array!(@tipologias) do |tipologia|
  json.extract! tipologia, :id, :nombre
  json.url tipologia_url(tipologia, format: :json)
end
