json.array!(@organizaciones) do |organizacion|
  json.extract! organizacion, :id, :nombre
  json.url organizacion_url(organizacion, format: :json)
end
