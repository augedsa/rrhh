json.array!(@permisologias_menu) do |permisologia_menu|
  json.extract! permisologia_menu, :id, :rol_id, :menu_id, :permiso
  json.url permisologia_menu_url(permisologia_menu, format: :json)
end
