json.array!(@edociviles) do |edocivil|
  json.extract! edocivil, :id, :nombre
  json.url edocivil_url(edocivil, format: :json)
end
