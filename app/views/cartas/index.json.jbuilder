json.array!(@cartas) do |carta|
  json.extract! carta, :id, :usuario_id, :prospecto_id, :fecha, :cuerpo, :respuesta, :url_adjunto
  json.url carta_url(carta, format: :json)
end
