json.array!(@habilidades) do |habilidad|
  json.extract! habilidad, :id, :nombre
  json.url habilidad_url(habilidad, format: :json)
end
