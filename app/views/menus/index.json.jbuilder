json.array!(@menus) do |menu|
  json.extract! menu, :id, :span_menu, :class_menu, :path_menu, :id_li_menu
  json.url menu_url(menu, format: :json)
end
