json.array!(@permisologia_configuraciones) do |permisologia_configuracion|
  json.extract! permisologia_configuracion, :id, :rol_id, :configuracion_id, :permiso
  json.url permisologia_configuracion_url(permisologia_configuracion, format: :json)
end
