json.array!(@objetivos) do |objetivo|
  json.extract! objetivo, :id, :titulo, :cuerpo, :fecha_inicio, :fecha_fin, :estado_id, :proyecto_id
  json.url objetivo_url(objetivo, format: :json)
end
