json.array!(@entrenamientos) do |entrenamiento|
  json.extract! entrenamiento, :id, :nombre
  json.url entrenamiento_url(entrenamiento, format: :json)
end
