require 'test_helper'

class NotasTelefonicasControllerTest < ActionController::TestCase
  setup do
    @nota_telefonicas = notas_telefonicas(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:notas_telefonicas)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create nota_telefonicas" do
    assert_difference('NotaTelefonicas.count') do
      post :create, nota_telefonicas: { fecha: @nota_telefonicas.fecha, fecha_recordatorio: @nota_telefonicas.fecha_recordatorio, nota_descripcion: @nota_telefonicas.nota_descripcion, prospecto_id: @nota_telefonicas.prospecto_id, usuario_id: @nota_telefonicas.usuario_id }
    end

    assert_redirected_to nota_telefonicas_path(assigns(:nota_telefonicas))
  end

  test "should show nota_telefonicas" do
    get :show, id: @nota_telefonicas
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @nota_telefonicas
    assert_response :success
  end

  test "should update nota_telefonicas" do
    patch :update, id: @nota_telefonicas, nota_telefonicas: { fecha: @nota_telefonicas.fecha, fecha_recordatorio: @nota_telefonicas.fecha_recordatorio, nota_descripcion: @nota_telefonicas.nota_descripcion, prospecto_id: @nota_telefonicas.prospecto_id, usuario_id: @nota_telefonicas.usuario_id }
    assert_redirected_to nota_telefonicas_path(assigns(:nota_telefonicas))
  end

  test "should destroy nota_telefonicas" do
    assert_difference('NotaTelefonicas.count', -1) do
      delete :destroy, id: @nota_telefonicas
    end

    assert_redirected_to notas_telefonicas_path
  end
end
