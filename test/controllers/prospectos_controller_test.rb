require 'test_helper'

class ProspectosControllerTest < ActionController::TestCase
  setup do
    @prospecto = prospectos(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:prospectos)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create prospecto" do
    assert_difference('Prospecto.count') do
      post :create, prospecto: { area_id: @prospecto.area_id, correo: @prospecto.correo, deudas: @prospecto.deudas, direccion: @prospecto.direccion, edad: @prospecto.edad, edocivil_id: @prospecto.edocivil_id, entrenamiento_id: @prospecto.entrenamiento_id, exstaff: @prospecto.exstaff, habilidad_id: @prospecto.habilidad_id, hijos: @prospecto.hijos, nombre: @prospecto.nombre, organizacion_id: @prospecto.organizacion_id, staff: @prospecto.staff, telefono: @prospecto.telefono, tipo_id: @prospecto.tipo_id }
    end

    assert_redirected_to prospecto_path(assigns(:prospecto))
  end

  test "should show prospecto" do
    get :show, id: @prospecto
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @prospecto
    assert_response :success
  end

  test "should update prospecto" do
    patch :update, id: @prospecto, prospecto: { area_id: @prospecto.area_id, correo: @prospecto.correo, deudas: @prospecto.deudas, direccion: @prospecto.direccion, edad: @prospecto.edad, edocivil_id: @prospecto.edocivil_id, entrenamiento_id: @prospecto.entrenamiento_id, exstaff: @prospecto.exstaff, habilidad_id: @prospecto.habilidad_id, hijos: @prospecto.hijos, nombre: @prospecto.nombre, organizacion_id: @prospecto.organizacion_id, staff: @prospecto.staff, telefono: @prospecto.telefono, tipo_id: @prospecto.tipo_id }
    assert_redirected_to prospecto_path(assigns(:prospecto))
  end

  test "should destroy prospecto" do
    assert_difference('Prospecto.count', -1) do
      delete :destroy, id: @prospecto
    end

    assert_redirected_to prospectos_path
  end
end
