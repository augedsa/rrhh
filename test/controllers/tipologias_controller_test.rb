require 'test_helper'

class TipologiasControllerTest < ActionController::TestCase
  setup do
    @tipologia = tipologias(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:tipologias)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create tipologia" do
    assert_difference('Tipologia.count') do
      post :create, tipologia: { nombre: @tipologia.nombre }
    end

    assert_redirected_to tipologia_path(assigns(:tipologia))
  end

  test "should show tipologia" do
    get :show, id: @tipologia
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @tipologia
    assert_response :success
  end

  test "should update tipologia" do
    patch :update, id: @tipologia, tipologia: { nombre: @tipologia.nombre }
    assert_redirected_to tipologia_path(assigns(:tipologia))
  end

  test "should destroy tipologia" do
    assert_difference('Tipologia.count', -1) do
      delete :destroy, id: @tipologia
    end

    assert_redirected_to tipologias_path
  end
end
