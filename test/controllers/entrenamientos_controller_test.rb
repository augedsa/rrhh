require 'test_helper'

class EntrenamientosControllerTest < ActionController::TestCase
  setup do
    @entrenamiento = entrenamientos(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:entrenamientos)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create entrenamiento" do
    assert_difference('Entrenamiento.count') do
      post :create, entrenamiento: { nombre: @entrenamiento.nombre }
    end

    assert_redirected_to entrenamiento_path(assigns(:entrenamiento))
  end

  test "should show entrenamiento" do
    get :show, id: @entrenamiento
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @entrenamiento
    assert_response :success
  end

  test "should update entrenamiento" do
    patch :update, id: @entrenamiento, entrenamiento: { nombre: @entrenamiento.nombre }
    assert_redirected_to entrenamiento_path(assigns(:entrenamiento))
  end

  test "should destroy entrenamiento" do
    assert_difference('Entrenamiento.count', -1) do
      delete :destroy, id: @entrenamiento
    end

    assert_redirected_to entrenamientos_path
  end
end
