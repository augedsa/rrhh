require 'test_helper'

class ObjetivosControllerTest < ActionController::TestCase
  setup do
    @objetivo = objetivos(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:objetivos)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create objetivo" do
    assert_difference('Objetivo.count') do
      post :create, objetivo: { cuerpo: @objetivo.cuerpo, estado_id: @objetivo.estado_id, fecha_fin: @objetivo.fecha_fin, fecha_inicio: @objetivo.fecha_inicio, proyecto_id: @objetivo.proyecto_id, titulo: @objetivo.titulo }
    end

    assert_redirected_to objetivo_path(assigns(:objetivo))
  end

  test "should show objetivo" do
    get :show, id: @objetivo
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @objetivo
    assert_response :success
  end

  test "should update objetivo" do
    patch :update, id: @objetivo, objetivo: { cuerpo: @objetivo.cuerpo, estado_id: @objetivo.estado_id, fecha_fin: @objetivo.fecha_fin, fecha_inicio: @objetivo.fecha_inicio, proyecto_id: @objetivo.proyecto_id, titulo: @objetivo.titulo }
    assert_redirected_to objetivo_path(assigns(:objetivo))
  end

  test "should destroy objetivo" do
    assert_difference('Objetivo.count', -1) do
      delete :destroy, id: @objetivo
    end

    assert_redirected_to objetivos_path
  end
end
