require 'test_helper'

class PermisologiasMenuControllerTest < ActionController::TestCase
  setup do
    @permisologia_menu = permisologias_menu(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:permisologias_menu)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create permisologia_menu" do
    assert_difference('PermisologiaMenu.count') do
      post :create, permisologia_menu: { menu_id: @permisologia_menu.menu_id, permiso: @permisologia_menu.permiso, rol_id: @permisologia_menu.rol_id }
    end

    assert_redirected_to permisologia_menu_path(assigns(:permisologia_menu))
  end

  test "should show permisologia_menu" do
    get :show, id: @permisologia_menu
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @permisologia_menu
    assert_response :success
  end

  test "should update permisologia_menu" do
    patch :update, id: @permisologia_menu, permisologia_menu: { menu_id: @permisologia_menu.menu_id, permiso: @permisologia_menu.permiso, rol_id: @permisologia_menu.rol_id }
    assert_redirected_to permisologia_menu_path(assigns(:permisologia_menu))
  end

  test "should destroy permisologia_menu" do
    assert_difference('PermisologiaMenu.count', -1) do
      delete :destroy, id: @permisologia_menu
    end

    assert_redirected_to permisologias_menu_path
  end
end
