require 'test_helper'

class PermisologiaConfiguracionesControllerTest < ActionController::TestCase
  setup do
    @permisologia_configuracion = permisologia_configuraciones(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:permisologia_configuraciones)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create permisologia_configuracion" do
    assert_difference('PermisologiaConfiguracion.count') do
      post :create, permisologia_configuracion: { configuracion_id: @permisologia_configuracion.configuracion_id, permiso: @permisologia_configuracion.permiso, rol_id: @permisologia_configuracion.rol_id }
    end

    assert_redirected_to permisologia_configuracion_path(assigns(:permisologia_configuracion))
  end

  test "should show permisologia_configuracion" do
    get :show, id: @permisologia_configuracion
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @permisologia_configuracion
    assert_response :success
  end

  test "should update permisologia_configuracion" do
    patch :update, id: @permisologia_configuracion, permisologia_configuracion: { configuracion_id: @permisologia_configuracion.configuracion_id, permiso: @permisologia_configuracion.permiso, rol_id: @permisologia_configuracion.rol_id }
    assert_redirected_to permisologia_configuracion_path(assigns(:permisologia_configuracion))
  end

  test "should destroy permisologia_configuracion" do
    assert_difference('PermisologiaConfiguracion.count', -1) do
      delete :destroy, id: @permisologia_configuracion
    end

    assert_redirected_to permisologia_configuraciones_path
  end
end
