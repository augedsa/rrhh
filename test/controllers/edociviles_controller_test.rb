require 'test_helper'

class EdocivilesControllerTest < ActionController::TestCase
  setup do
    @edocivil = edociviles(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:edociviles)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create edocivil" do
    assert_difference('Edocivil.count') do
      post :create, edocivil: { nombre: @edocivil.nombre }
    end

    assert_redirected_to edocivil_path(assigns(:edocivil))
  end

  test "should show edocivil" do
    get :show, id: @edocivil
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @edocivil
    assert_response :success
  end

  test "should update edocivil" do
    patch :update, id: @edocivil, edocivil: { nombre: @edocivil.nombre }
    assert_redirected_to edocivil_path(assigns(:edocivil))
  end

  test "should destroy edocivil" do
    assert_difference('Edocivil.count', -1) do
      delete :destroy, id: @edocivil
    end

    assert_redirected_to edociviles_path
  end
end
