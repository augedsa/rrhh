require 'test_helper'

class ClaveTest < ActionMailer::TestCase
  test "olvido_clave" do
    mail = Clave.olvido_clave
    assert_equal "Olvido clave", mail.subject
    assert_equal ["to@example.org"], mail.to
    assert_equal ["from@example.com"], mail.from
    assert_match "Hi", mail.body.encoded
  end

end
